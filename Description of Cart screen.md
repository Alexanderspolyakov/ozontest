# Description of "Cart" screen with at least one item in cart

1. At the very top, there is a centered title "Cart."
2. A delivery location selection menu with the format "[Delivery Location Type] · [Delivery Address (starting from the street)]."
3. The "Select All" checkbox. If the setting is active, the "Delete Selected" button is available.
4. "Ozon Delivery" text.
5. A row with texts "Courier" with a truck icon on the left and the text "Pickup" with a droplet-shaped pin icon on the left. 
6. A block with the items in the cart.
   1. A checkbox to select the item for performing group actions.
   2. A product image (or an image placeholder).
   3. The text "[The final price with discount (if applicable) and Ozon Card applied [a currency symbol] with Ozon Card]".
   4. "[The final price with discount (if applicable) without Ozon Card applied] [a currency symbol]".
   5. The field with the product description.
   6. Product color. The text: "color [the color name]."
   7. An emblem indicating the monthly installment payment when purchasing on installments, with the text "In Installments [the sum of the monthly installment payment] / month."
   8. A row with the buttons "Favorites" with a heart icon and "Delete" with a basket icon, as well as a parameter indicating the quantity of the product with the text "[The quantity of the product in pieces] pcs." and an icon with corners pointing down and up.

7. "Your Cart" block:
   1. The text: "[the total quantity of items in the cart (pcs)] item(s) · [the total weight of items in the cart in kilograms] kg."
   2. The text: "Items ([the total quantity of items in the cart (pcs)]) [the total sum of items in the cart excluding discounts]."
   3. The text Discount: [the total amount of discounts on all items in the cart] (if any item comes with discount).
   4. The link: "Learn More."
   5. If Ozon card is applicable: the text "With Ozon Card [the final price with Ozon Card applied and including discount (if applicable)] [a currency symbol]."
   6. If Ozon card is applicable: the text: "Without Ozon Card [the final price without Ozon Card and including discount (if applicable)] [a currency symbol]."
   7. The text "Cart subtotal [The total price of all items in the cart]"
   8. The button "Proceed to Checkout". When the button is out of the user's visible area, elements e, f, and g appear at the bottom of the screen as a separate block. It disappears when a user again views this button.
   9. The text: "Available delivery methods and time can be selected during the checkout process."

8. The "Recently Viewed" block displaying products viewed by the user. For each product, the following is displayed:
   1. An Image (or an image placeholder)
   2. The "Add to Favorites" button in a form of the heart icon in the top right corner of the image
   3. The Discount in per cent in the bottom left corner of the image (if applicable)
   4. The cost: [the final price with discount and without Ozon card applied]
   5. Item description